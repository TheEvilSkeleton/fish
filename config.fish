set -g _tide_fish_right_prompt_display

alias l "ls -a --hyperlink --color=always"
alias ls "ls -a --hyperlink --color=always"

alias bundle "toolbox run -c debian-jekyll bundle"
alias jekyll "toolbox run -c debian-jekyll jekyll"
alias gem "toolbox run -c debian-jekyll gem"
